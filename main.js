function createElementsList(array, parent = document.body) {

    const ul = document.createElement('ul');
    parent.append(ul)

    for (let elem of array){
        const li = document.createElement('li')
        li.append(elem)
        ul.append(li)
    }
}
createElementsList(["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"]);
